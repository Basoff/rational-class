#include "rational.h"

//Initialisation
Rational::Rational()
{
	numer = 0;
	denom = 1;
}

Rational::Rational(int number)
{
	numer = number;
	denom = 1;
}

Rational::Rational(int n, int d)
{
	numer = n;
	denom = d;
}

Rational::Rational(long long n, long long d)
{
	numer = n;
	denom = d;
}

Rational Rational::Rat(double d)
{
	long long v = *(long long *) &d;
	long long order = (int)((v >> 52) & 0x7FF) - 1023;
	long long m = v & 0xFFFFFFFFFFFFF | 0x10000000000000;
	
	this->numer = order >= 0 ? (m << order) : (m >> (-order));
	if (d < 0)
	{
		this->numer = -this->numer;
	}
	this->denom = 1LL << 52;
//cout<<endl<<"Rationalization:	"<<numer<<"	"<<denom<<endl;
	simplify();
	return *this;
}
//Arithmetics
Rational& Rational::operator += (const Rational& r)
{
	numer = (numer*r.denom + denom*r.numer);
	denom *= r.denom;
	simplify();
	return *this;
}

void Rational::simplify()
{
//cout<<numer<<"	"<<denom<<endl;
	long long vremn, vremd;
	if (numer > 0)
		vremn = numer;
	else
		vremn = -numer;
//cout<<endl<<endl<<"vremn:"<<vremn<<endl;
	if (denom > 0)
		vremd = denom;
	else
		vremn = -denom;
		
//cout<<"vremd:"<<vremd<<endl;
	if (vremn != 1 && vremd != 1)
	{
		while (vremn != vremd)
		{
//cout<<"Simplification:	"<<vremn<<"	"<<vremd<<endl;
			if (vremd > vremn)
			{
				vremd -= vremn;
			}
			else
			{
				vremn -= vremd;
			}
		}
			
		numer /= vremn;
		denom /= vremn;
//cout<<"Simplyfied:	"<<numer<<"/"<<denom<<endl;		
	}
}

void Rational::invert()
{
	int ext;
	ext = numer;
	numer = denom;
	denom = ext;
}

Rational Rational::operator + (const Rational &r) const
{
	Rational res (*this);
	return res += r;
}

Rational Rational::operator -() const
{
	Rational r(-numer, denom);
	return r;
}  

Rational& Rational::operator -= (const Rational& r)
{
	return (*this += (-r));
}

Rational& Rational::operator ++()
{
	numer += denom;
	return *this;
}

Rational Rational::operator ++(int)
{
	Rational r(*this);
	numer += denom;
	return r;
}

Rational& Rational::operator *= (const Rational& r){
	numer *= r.numer;
	denom *= r.denom;
	if (denom != 1) simplify(); 
	return *this;
}

Rational& Rational::operator *= (const int r){
	numer *= r;
	if (denom != 1) simplify(); 
	return *this;
}

Rational Rational::operator * (const Rational& r) const{
	Rational res (*this);
	return res *= r;
}

Rational Rational::operator * (const int r) const
{
	Rational res(*this);
	res *= r;
	return res;
} 

Rational& Rational::operator /= (const Rational& r){
	numer * r.denom;
	denom * r.numer;
	if (denom != 1) simplify(); ;
	return *this;
} 

Rational Rational::operator / (const Rational& r) const{
	Rational res (*this);
	Rational r2 (r);
	r2.invert();
	return res *= r2;
}

Rational Rational::sr()
{
	double n = numer;
	double d = denom;
//cout<<endl<<"Basic numbers:	"<<n<<"	"<<d<<endl;
//cout<<endl<<"Square roots:	"<<sqrt(n)<<"	"<<sqrt(d)<<endl;
	return Rat(sqrt(n)/sqrt(d));
}


//Comparison
void Rational::operator = (const Rational& r)
{
	numer = r.numer;
	denom = r.denom;
}

bool Rational::operator == (const Rational& r) const
{
	return (numer == r.numer) && (denom == r.denom);
}

bool Rational::operator != (const Rational& r) const
{
	return !(*this == r);
}

bool Rational::operator > (const Rational& r) const
{
	return (numer * r.denom > r.numer * denom);
}


bool Rational::operator >= (const Rational& r) const
{
	return (numer * r.denom >= r.numer * denom);
}


bool Rational::operator < (const Rational& r) const
{
	return (numer * r.denom < r.numer * denom);
}


bool Rational::operator <= (const Rational& r) const
{
	return (numer * r.denom <= r.numer * denom);
}

//Type changes
Rational::operator int() const
{
	return numer / denom;
}

Rational::operator double() const
{
	return ((double)numer)/denom;
} 

//Iostream
istream& operator >> (istream& in, Rational& r)
{
	in >> r.numer >> r.denom;
	return in;
}

ostream& operator << (ostream& out, const Rational& r)
{
	if (r.denom > 1)
		out << r.numer << "/" << r.denom;
	else
		out << r.numer;
	return out;
}
