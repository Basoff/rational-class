#include "rational.h"
#include <iostream>
using namespace std;

int main()
{
	//tests
	/*Rational a(1,3), b(-1,5);
	cout << "OK!\n";
	cout<< "a = " << a << "\nb = " << b << "\na + b = " << a + b << "\na - b = " << a - b <<"\na * b = " << a * b << "\na / b = " << a/b <<endl;
	cout<<"a == b:	"; if (a == b) cout<<"true"<<endl; else cout<<"false"<<endl;
	cout<<"a >= b:	"; if (a >= b) cout<<"true"<<endl; else cout<<"false"<<endl;
	cout<<"a <= b:	"; if (a <= b) cout<<"true"<<endl; else cout<<"false"<<endl;
	cout<<"a > b:	"; if (a > b) cout<<"true"<<endl; else cout<<"false"<<endl;
	cout<<"a < b:	"; if (a < b) cout<<"true"<<endl; else cout<<"false"<<endl;
	cout<<"a != b:	"; if (a != b) cout<<"true"<<endl; else cout<<"false"<<endl;*/

	//square equasion
	//ax^2 + bx + c = 0;
	Rational A, B, C;
	cout<<"Enter two numbers for A:";cin >> A;
	cout<<"\nEnter two numbers for B:";cin >> B;
	cout<<"\nEnter two numbers for C:";cin >> C;
	
	cout<<endl;
	cout<<A<<"*x^2 + "<<B<<"*x + "<<C<<endl;
	
	Rational D = B*B +(-(A*C)*4);
	if (D >= Rational(0))
	{
		Rational x1 = (-B + D.sr())/(A*2);
		cout<<"X1="<<(double)x1<<endl;
			
		Rational x2 = (-B + -D)/(A*2);
		cout<<"X2="<<(double)x2<<endl;
	}
	else cout<<"Discriminant is below zero. No real solutions.\n";
	return 0;
}