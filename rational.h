#ifndef _RATIONAL_H
#define _RATIONAL_H

#include <iostream>
#include <cmath>
using namespace std;

class Rational{

	void simplify();
	void invert();

	public:

	long long numer;
	long long denom;
	//Intialisation
	Rational();
	Rational(int number);
	Rational(int n, int d);
	Rational(long long n, long long d);
	//Arithmetical operations
	Rational operator + (const Rational& r) const;
	Rational& operator += (const Rational& r);
	Rational operator - () const;
	Rational& operator -= (const Rational& r);
	Rational& operator ++();
	Rational operator ++(int);
	Rational& operator *= (const Rational& r);
	Rational& operator *= (const int r);
	Rational operator * (const Rational& r) const;
	Rational operator * (const int r) const;
	Rational& operator /= (const Rational& r);
	Rational operator / (const Rational& r) const;
	Rational sr();
	Rational Rat(double d);
	//Comparison
	void operator = (const Rational& r);
	bool operator == (const Rational& r) const;
	bool operator != (const Rational& r) const;
	bool operator > (const Rational& r) const;
	bool operator >= (const Rational& r) const;
	bool operator < (const Rational& r) const;
	bool operator <= (const Rational& r) const;
	//Type change
	operator int() const;
	operator double() const;
	//Iostream
	friend istream& operator >> (istream& in, Rational& r);
	friend ostream& operator << (ostream& out, const Rational& r);
};

#endif
